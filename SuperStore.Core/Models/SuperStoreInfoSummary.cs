﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperStore.Core.Models
{
    public class SuperStoreInfoSummary
    { 
        public string Season { get; set; } 
        public string Region { get; set; } 

        public string Category { get; set; } 
        public decimal SalesTotal { get; set; } 
    }
}
