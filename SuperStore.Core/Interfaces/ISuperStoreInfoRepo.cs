﻿using SuperStore.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperStore.Core.Interfaces
{
    public interface ISuperStoreInfoRepo
    {
        Task<List<SuperStoreInfo>> GetSuperStoreInfoList(bool refresh = false);
        Task<List<SuperStoreInfoSummary>> GetSuperStoreSummaryInfoList(bool refresh = false);
        Task<List<SuperStoreInfoSummary>> GetBestProductsLocationSeasonList(bool refresh = false);
    }
}
