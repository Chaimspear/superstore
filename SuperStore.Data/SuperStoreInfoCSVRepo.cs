﻿using Microsoft.VisualBasic.FileIO;
using SuperStore.Core.Interfaces;
using SuperStore.Core.Models;

namespace SuperStore.Data
{
    public class SuperStoreInfoCSVRepo : ISuperStoreInfoRepo
    { 
        private List<SuperStoreInfo> _superStoreInfoList;
        private string GetSeason(DateOnly theDate)
        {
            var theSeason = string.Empty;
            switch (theDate.Month)
            {
                case int i when i >= 1 & i <= 3:
                    theSeason = "Winter";
                    break;
                case int i when i >= 3 & i <= 6:
                    theSeason = "Spring";
                    break;
                case int i when i >= 6 & i <= 9:
                    theSeason = "Summer";
                    break;
                case int i when i >= 9 & i <= 12:
                    theSeason = "Fall";
                    break;

            }
            return theSeason;
        }

        public async Task<List<SuperStoreInfoSummary>> GetBestProductsLocationSeasonList(bool refresh = false)
        {
            var superStoreSummaryInfoList = await GetSuperStoreSummaryInfoList(refresh);

            var regionSeasonBestSuperStoreSales =
                from salesSum in superStoreSummaryInfoList
                group salesSum by new { salesSum.Season, salesSum.Region } into salesSumGroup
                select new SuperStoreInfoSummary
                {
                    Region = salesSumGroup.Key.Region,
                    Season = salesSumGroup.Key.Season,
                    SalesTotal = salesSumGroup.Max(x => x.SalesTotal),
                };

            var bestProductsLocationSeasonList = from bestsales in regionSeasonBestSuperStoreSales
                                                 join sumSales in superStoreSummaryInfoList
                                                 on new { bestsales.Region, bestsales.Season, bestsales.SalesTotal } equals
                                                  new { sumSales.Region, sumSales.Season, sumSales.SalesTotal }

                                                 select new SuperStoreInfoSummary
                                                 {
                                                     Season = bestsales.Season,
                                                     Region = bestsales.Region,
                                                     Category = sumSales.Category,
                                                     SalesTotal = bestsales.SalesTotal
                                                 };

            return bestProductsLocationSeasonList.ToList();
        }

        public async Task<List<SuperStoreInfoSummary>> GetSuperStoreSummaryInfoList(bool refresh = false)
        {

            var superStoreInfoList = await GetSuperStoreInfoList(refresh);

            var superStoreInfoSummaryList = superStoreInfoList.GroupBy(r => new
            {
                r.Region,
                r.Category,
                Season = GetSeason(r.OrderDate)
            }).
               Select(g => new SuperStoreInfoSummary
               {
                   Season = g.Key.Season,
                   Region = g.Key.Region,
                   Category = g.Key.Category,
                   SalesTotal = g.Sum(a => a.Sales)
                   //RecipeCount = g.Count() 
               });

            return superStoreInfoSummaryList.ToList();
        }
        
        public async Task<List<SuperStoreInfo>> GetSuperStoreInfoList(bool refresh = false)
        {
            if (refresh || _superStoreInfoList == null)
            {
                var superStoreInfoList = new List<SuperStoreInfo>();
                var folderFile = $@"{System.IO.Directory.GetCurrentDirectory()}\wwwroot\DataFolder\";
                var inputFilePath = $"{folderFile}Sample - Superstore.csv";

                StreamReader reader = null;
                if (File.Exists(inputFilePath))
                {
                    reader = new StreamReader(File.OpenRead(inputFilePath));
                    if (!reader.EndOfStream)//skip 1st line
                    { reader.ReadLine(); }

                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        //var values = line.Split(',');

                        TextFieldParser parser = new TextFieldParser(new StringReader(line));

                        parser.HasFieldsEnclosedInQuotes = true;
                        parser.SetDelimiters(",");
                        var values = parser.ReadFields();
                        const int ORDER_ID_COL = 1;
                        const int ORDER_DATE_COL = 2;
                        const int SHIP_DATE_COL = 3;
                        const int SHIP_MODE_COL = 4;
                        const int CUSTOMER_ID_COL = 5;
                        const int CUSTOMER_NAME_COL = 6;
                        const int SEGMENT_COL = 7;
                        const int COUNTRY_COL = 8;
                        const int CITY_COL = 9;
                        const int STATE_COL = 10;
                        const int POST_CODE_COL = 11;
                        const int REGION_COL = 12;
                        const int PRODUCT_ID_COL = 13;
                        const int CATGEGORY_COL = 14;
                        const int SUB_CATGEGORY_COL = 15;
                        const int PRODUCT_NAME_COL = 16;
                        const int SALES_COL = 17;
                        const int QTY_COL = 18;
                        const int DISCOUNT_COL = 19;
                        const int PROFIT_COL = 20;

                        var superStoreInfoRec = new SuperStoreInfo
                        {
                            OrderId = values[ORDER_ID_COL],
                            CustomerName = values[CUSTOMER_NAME_COL],
                            CustomerId = values[CUSTOMER_ID_COL],
                            OrderDate = DateOnly.FromDateTime(Convert.ToDateTime(values[ORDER_DATE_COL])),
                            ShipDate = DateOnly.FromDateTime(Convert.ToDateTime(values[SHIP_DATE_COL])),
                            ShipMode = values[SHIP_MODE_COL],
                            Segment = values[SEGMENT_COL],
                            Country = values[COUNTRY_COL],
                            City = values[CITY_COL],
                            State = values[STATE_COL],
                            PostalCode = values[POST_CODE_COL],
                            Region = values[REGION_COL],
                            ProductId = values[PRODUCT_ID_COL],
                            Category = values[CATGEGORY_COL],
                            SubCategory = values[SUB_CATGEGORY_COL],
                            ProductName = values[PRODUCT_NAME_COL],
                            Sales = Convert.ToDecimal(values[SALES_COL]),
                            Quantity = Convert.ToInt32(values[QTY_COL]),
                            Discount = Convert.ToDecimal(values[DISCOUNT_COL]),
                            Profit = Convert.ToDecimal(values[PROFIT_COL])
                        };
                        superStoreInfoList.Add(superStoreInfoRec);
                    }
                }
                else
                {
                    Console.WriteLine("File doesn't exist");
                }
                _superStoreInfoList = superStoreInfoList;
            }
             return _superStoreInfoList;
        }

        public List<SuperStoreInfo> GetSuperStoreInfoListEXCEL() //keeping for later reference
        {
            var superStoreInfoList = new List<SuperStoreInfo>();
            //throw new NotImplementedException();
            //var mardownFile = System.IO.File.ReadAllText($"{System.IO.Directory.GetCurrentDirectory()}{@"\wwwroot\news.md"}");
            var folderFile = $@"{System.IO.Directory.GetCurrentDirectory()}\wwwroot\";
            var inputFile = new FileInfo($"{folderFile}fileToRead.xlsx");

            // Create an instance of Fast Excel
            using (FastExcel.FastExcel fastExcel = new FastExcel.FastExcel(inputFile, true))
            {
                foreach (var worksheet in fastExcel.Worksheets)
                {
                    Console.WriteLine(string.Format("Worksheet Name:{0}, Index:{1}", worksheet.Name, worksheet.Index));

                    //To read the rows call read
                    worksheet.Read();
                    var rows = worksheet.Rows.Skip(1).ToArray();
                    //Do something with rows
                    Console.WriteLine(string.Format("Worksheet Rows:{0}", rows.Count()));

                    foreach (var theRow in rows)
                    {
                        var cells = theRow.Cells.ToArray();
                        var goa = cells[0];
                        const int ORDER_ID_COL = 1;
                        const int ORDER_DATE_COL = 2;
                        const int CUSTOMER_ID_COL = 5;
                        const int CUSTOMER_NAME_COL = 6;

                        var superStoreInfoRec = new SuperStoreInfo
                        {
                            OrderId = cells[ORDER_ID_COL].Value.ToString(),
                            CustomerName = cells[CUSTOMER_NAME_COL].Value.ToString()
                        };
                        superStoreInfoList.Add(superStoreInfoRec);
                    }
                }
            }
            return superStoreInfoList;
        }
    }
}
